package com.example;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class Example6
	{
	    public static void main(String[] args)
	    {
	        try
	        {
	            SerializationDemo s=new SerializationDemo("Sushmitha",10,20);
	            FileOutputStream f=new FileOutputStream("C:\\Users\\suthangellapally\\Desktop\\Sushmitha.txt");
	            ObjectOutputStream o=new ObjectOutputStream(f);
	            o.writeObject(s);
	            o.flush();
	            f.close();
	        }
	        catch(Exception e)
	        {
	            e.printStackTrace();
	        }
	        try
	        {
	            FileInputStream f=new FileInputStream("C:\\Users\\suthangellapally\\Desktop\\Sushmitha.txt");
	            ObjectInputStream o=new ObjectInputStream(f);
	            SerializationDemo s=(SerializationDemo)o.readObject();
	            System.out.println(s.a+" "+s.b+" "+s.c);
	            o.close();
	        }
	        catch(Exception e)
	        {
	            e.printStackTrace();
	        }
	    }
	}


