package com.example;
public class Example4 extends Thread
{
	public void run()
	{
		if(Thread.currentThread().isDaemon())
		{
			System.out.println("daemon thread work ");
		}
		else
		{
			System.out.println("user thread work ");
		}
	}
	
	public static void main(String args[])
	{
		Example4 t1=new Example4();
		Example4 t2=new Example4();
		Example4 t3=new Example4();
		t1.setDaemon(true);
		t1.start();
		t2.start();
		t3.start();
	}
}

