package com.example;

import java.io.Serializable;

public class SerializationDemo implements Serializable
{
    String a;
    int b;
    transient int c;
    public SerializationDemo(String a,int b,int c)
    {
        this.a=a;
        this.b=b;
        this.c=c;
    }
}

