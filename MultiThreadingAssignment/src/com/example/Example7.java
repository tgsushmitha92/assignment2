package com.example;


import java.util.Arrays;
public class Example7
{
	class bubbleSort
	{
		public int[] dataSorting(int[] a)
		{
			for(int i=0;i<a.length;i++)
			{
				for(int j=i+1;j<a.length;j++)
				{
					if(a[i]>a[j])
					{
						int temp=a[i];
						a[i]=a[j];
						a[j]=temp;
						
					}
				}
			}
			return a;
		}
	}
	public static void main(String args[])
	{
		int a[]= {7,3,1,9,2};
		bubbleSort obj=new Example7().new bubbleSort();
		System.out.println("Array before sorting"+Arrays.toString(a));
		System.out.println("Array after sorting"+Arrays.toString(obj.dataSorting(a)));
		
	}
}

